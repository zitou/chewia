import pygame        #import de pygame
from Debug import Debug     #sert à prendre les données de la classe
from Plateau import Plateau
from Pion import Pion
from Position import Position

HAUTEUR = 800
LARGEUR = 800
DEBUG = True        #active la fonction debug, donc les commentaires, si debug=false, les commentaires ne sont pas lisible

def main():
    continuer = True
    colonne=-1
    ligne=-1
    GREEN = (0,250,0)

    Debug().setValeur(DEBUG)
    pygame.init()
    plateau = Plateau(HAUTEUR, LARGEUR)

    while continuer:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:       #initialise la fonction quit
               	continuer = False
               	pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:   #Si clic gauche
                                        #On change les coordonnées du perso
                    click_x = event.pos[0]
                    click_y = event.pos[1]

                    plateau.afficher_position_click(click_x, click_y)

                    plateau.surligner_case(click_x, click_y)

                    plateau.changer_position_pieces(click_x, click_y)
main()