import os
from Debug import Debug
from Couleur import Couleur
from Pieces import Pieces

class Cavalier(Pieces):
    couleur = ""

    def __init__(self, couleur):
        Debug.log_method("Cavalier: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd()+"\\resources\\imagesPions\\cav_noir.png"
        else:
            self.image = os.getcwd()+"\\resources\\imagesPions\\cav_blanc.png"