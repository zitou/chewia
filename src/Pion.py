import os
import pygame
from Debug import Debug
from Pieces import Pieces
from Couleur import Couleur
from Case import Case
from Position import Position

class Pion(Pieces):

    #GREEN = (0,250,0)
    def __init__(self, couleur):
        Debug.log_method("Pion: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd()+"\\resources\\imagesPions\\pion_noir.png"
        else:
                 self.image = os.getcwd()+"\\resources\\imagesPions\\pion_blanc.png"
