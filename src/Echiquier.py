""" @classname: Echiquier
"""
from Debug import Debug
from Position import Position
from Case import Case

class Echiquier():
    """ @class: Echiquier
        Represente l'echiquer du jeu d'echec, les cases.
        Initialise les cases A1 -> H8
        Affecte les cases ainsi initialisées au tableau 'contenu'
        Commentaires: Il existe 3 types de positionnement sur l'echiquier
        -1. Position.XY qui donne le numero de la case sur l'echiquier.
            Par exemple: Position.D4
        -2. Position par un couple (x, y) ou (0<x<7 et 0<y<7), x et y donnant
            les indices du tableau à 2 dimensions des cases (x=colonnes et y=rangée)
        -3. Les coordonnees relatives par rapport à la fenetre echiquier donnant
            l'abcisse et l'ordonnée en nombre de pixel 
    """
    cases = {}
    contenu = [[[] for i in range(8)] for i in range(8)]
    hauteur = 0
    largeur = 0

    def __init__(self, hauteur, largeur):
        """ @method: init
            But: Initialiser l'echiquier:
                initialisation des cases (affectation des positions sous forme Position.A1)
                initialisation du tableau 'contenu' qui contient toutes les 64 cases
        """
        Debug.log_method("Echiquier: init: >>>")
        self.hauteur = hauteur
        self.largeur = largeur

        self.cases[0, 0] = Position.A1
        self.cases[1, 0] = Position.A2
        self.cases[2, 0] = Position.A3
        self.cases[3, 0] = Position.A4
        self.cases[4, 0] = Position.A5
        self.cases[5, 0] = Position.A6
        self.cases[6, 0] = Position.A7
        self.cases[7, 0] = Position.A8

        self.cases[0, 1] = Position.B1
        self.cases[1, 1] = Position.B2
        self.cases[2, 1] = Position.B3
        self.cases[3, 1] = Position.B4
        self.cases[4, 1] = Position.B5
        self.cases[5, 1] = Position.B6
        self.cases[6, 1] = Position.B7
        self.cases[7, 1] = Position.B8

        self.cases[0, 2] = Position.C1
        self.cases[1, 2] = Position.C2
        self.cases[2, 2] = Position.C3
        self.cases[3, 2] = Position.C4
        self.cases[4, 2] = Position.C5
        self.cases[5, 2] = Position.C6
        self.cases[6, 2] = Position.C7
        self.cases[7, 2] = Position.C8
        
        self.cases[0, 3] = Position.D1
        self.cases[1, 3] = Position.D2
        self.cases[2, 3] = Position.D3
        self.cases[3, 3] = Position.D4
        self.cases[4, 3] = Position.D5
        self.cases[5, 3] = Position.D6
        self.cases[6, 3] = Position.D7
        self.cases[7, 3] = Position.D8

        self.cases[0, 4] = Position.E1
        self.cases[1, 4] = Position.E2
        self.cases[2, 4] = Position.E3
        self.cases[3, 4] = Position.E4
        self.cases[4, 4] = Position.E5
        self.cases[5, 4] = Position.E6
        self.cases[6, 4] = Position.E7
        self.cases[7, 4] = Position.E8

        self.cases[0, 5] = Position.F1
        self.cases[1, 5] = Position.F2
        self.cases[2, 5] = Position.F3
        self.cases[3, 5] = Position.F4
        self.cases[4, 5] = Position.F5
        self.cases[5, 5] = Position.F6
        self.cases[6, 5] = Position.F7
        self.cases[7, 5] = Position.F8

        self.cases[0, 6] = Position.G1
        self.cases[1, 6] = Position.G2
        self.cases[2, 6] = Position.G3
        self.cases[3, 6] = Position.G4
        self.cases[4, 6] = Position.G5
        self.cases[5, 6] = Position.G6
        self.cases[6, 6] = Position.G7
        self.cases[7, 6] = Position.G8

        self.cases[0, 7] = Position.H1
        self.cases[1, 7] = Position.H2
        self.cases[2, 7] = Position.H3
        self.cases[3, 7] = Position.H4
        self.cases[4, 7] = Position.H5
        self.cases[5, 7] = Position.H6
        self.cases[6, 7] = Position.H7
        self.cases[7, 7] = Position.H8

        colonne = ("A", "B", "C", "D", "E", "F", "G", "H")
        rangee = ("1", "2", "3", "4", "5", "6", "7", "8")
        for i in range(8):          # définie la taille de la case
            for j in range(8):
                position = self.change_string_to_case(colonne[j]+rangee[i])
                case = Case(position, self.hauteur - (i+1)*self.hauteur//8, j*self.largeur//8)
                self.contenu[i][j] = case
    
    def change_string_to_case(self, nom):
        """ @method: change_string_to_case
            But: Convertit la string D4 en PositionD4
            Entree: Une string sous la forme A1
            Sortie: Une position sous la forme Position.A1
        """
        for i in Position:
            if (Position(i).name == nom):
                return Position(i)

    def get_position(self, position):
        return list(self.cases.keys())[list(self.cases.values()).index(position)]

    def get_case_position(self, abscisse, ordonnee):
        """ @method: get_case_position
            But: Donner le nom de la case cliquée sous la forme Position.XY (par exemple Position.D3)
            Entree: position abcisse/ordonnee du tableau (0<x<7 et 0<y<7)
            Sortie: position sous la forme Position.XY (par exemple Position.D3)
        """
        colonne = ("A", "B", "C", "D", "E", "F", "G", "H")
        rangee = ("1", "2", "3", "4", "5", "6", "7", "8")
        position = self.change_string_to_case(colonne[abscisse]+rangee[7-ordonnee])
        return position