import pygame
import os
from Debug import Debug

class Fenetre:

    def __init__(self, hauteur, largeur):           #initialise la fenetre
        Debug.log_method("Fenetre: __init__: >>>")
        
        self.hauteur = hauteur
        self.largeur = largeur
        blanc = (0, 0, 0)

        self.fenetre = pygame.display.set_mode((largeur, hauteur))
        self.fenetre.fill(blanc)
        pygame.display.set_caption("Jeu d'Echec - Développé par Sarah & Laureline")
        fond = pygame.image.load(os.getcwd()+"\\resources\\imagesPions\\echiquier.png").convert()           #image de fond
        fond = pygame.transform.scale(fond, (largeur, hauteur))

        self.fenetre.blit(fond, (0, 0))

        pygame.display.flip()

    def affichePieces(self, piece):               #
        pieces = pygame.image.load(piece.image).convert()
        couleur_fond = pieces.get_at((0, 0))
        pieces.set_colorkey(couleur_fond)
        taille_pieces = pygame.transform.scale(pieces, (80, 80))        #définie une même taille pour toutes les pièces
        position = piece.position
    
        self.fenetre.blit(taille_pieces,(position.case_largeur, position.case_hauteur))
        pygame.display.flip()

    def blit(self, taille, rectX, rectY):       #définie
        self.fenetre.blit(taille, (rectX, rectY))

    def surligner_case(self, echiquier, position):
        pos_x = position[0]
        pos_y = position[1]
        color_green = (0, 255, 0)
        size = (self.hauteur//8, self.largeur//8)
        rect_image = pygame.Surface(size).convert()
        rect_image.set_alpha(50)
        rect_image.fill(color_green)

        rect = pygame.Rect(echiquier.contenu[pos_x][pos_y].case_hauteur, echiquier.contenu[pos_x][pos_y].case_largeur, self.hauteur//8, self.largeur//8)
        pygame.draw.rect(rect_image, color_green, rect)
        self.fenetre.blit(rect_image, (echiquier.contenu[pos_x][pos_y].case_largeur, echiquier.contenu[pos_x][pos_y].case_hauteur))
        pygame.display.update()
    