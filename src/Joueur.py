from Debug import Debug
from Tour import Tour
from Cavalier import Cavalier
from Fou import Fou
from Roi import Roi
from Reine import Reine
from Pion import Pion

class Joueur:

    def __init__(self, couleur):  
        Debug.log_method("Joueur: __init__: >>>")

        self.tour1 = Tour(couleur)	#en fonction du joueur et de la couleur -> tour1 sera soit blanc soit noir
        self.tour2 = Tour(couleur)	#voir Plateau - joueur 1 = piece blanche et joueur 2 = piece noir

        self.cavalier1 = Cavalier(couleur)
        self.cavalier2 = Cavalier(couleur)
        
        self.fou1 = Fou(couleur)
        self.fou2 = Fou(couleur)

        self.roi = Roi(couleur)

        self.reine = Reine(couleur)

        self.pion1 = Pion(couleur)
        self.pion2 = Pion(couleur)
        self.pion3 = Pion(couleur)
        self.pion4 = Pion(couleur)
        self.pion5 = Pion(couleur)
        self.pion6 = Pion(couleur)
        self.pion7 = Pion(couleur)
        self.pion8 = Pion(couleur)