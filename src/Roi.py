import os
from Debug import Debug
from Couleur import Couleur
from Pieces import Pieces

class Roi(Pieces):
    couleur = ""

    def __init__(self, couleur):
        Debug.log_method("Roi: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd()+"\\resources\\imagesPions\\roi_noir.png"
        else:
            self.image = os.getcwd()+"\\resources\\imagesPions\\roi_blanc.png"