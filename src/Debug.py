class Debug:
    valeur = False

    @staticmethod           #staticmethod = se lance qu'une seule fois
    def getValeur():
        return Debug.valeur

    @staticmethod       
    def setValeur(val):
        Debug.valeur = val

    @staticmethod
    def log_method(commentaire):        
        if (Debug.valeur):
            print(commentaire)      #impression des commentaires