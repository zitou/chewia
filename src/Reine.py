import os
from Debug import Debug
from Couleur import Couleur
from Pieces import Pieces

class Reine(Pieces):
    couleur = ""

    def __init__(self, couleur):
        Debug.log_method("Reine: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd()+"\\resources\\imagesPions\\reine_noir.png"
        else:
            self.image = os.getcwd()+"\\resources\\imagesPions\\reine_blanche.png"