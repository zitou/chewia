import os
from Debug import Debug
from Couleur import Couleur
from Pieces import Pieces

class Tour(Pieces):
    couleur = ""

    def __init__(self, couleur):
        Debug.log_method("Tour: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd() + "\\resources\\imagesPions\\tour_noir.png"	#si couleur noir - charge l'image de la piece en noir
        else:
            self.image = os.getcwd() + "\\resources\\imagesPions\\tour_blanc.png"	#sinon charge l'image blanche