import os
from Debug import Debug
from Couleur import Couleur
from Pieces import Pieces

class Fou(Pieces):
    couleur = ""

    def __init__(self, couleur):
        Debug.log_method("Fou: __init__: >>>")
        self.couleur = couleur
        if (couleur == Couleur.NOIR):
            self.image = os.getcwd()+"\\resources\\imagesPions\\fou_noir.png"
        else:
            self.image = os.getcwd()+"\\resources\\imagesPions\\fou_blanc.png"