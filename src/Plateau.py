import pygame
from Debug import Debug	#importation
from Position import Position
from Case import Case
from Echiquier import Echiquier
from Pieces import Pieces

from Fenetre import Fenetre
from Joueur import Joueur
from Couleur import Couleur

class Plateau:
    dico = {}
    
    GREEN=(0,255,0)

    def __init__(self, hauteur, largeur):
        Debug.log_method("Plateau:__init__: >>>")
		#self renvoie a la class, dit que s'est un attribut de celle-ci, il permet de distinguer l'attribut d'une à une variable normale
        self.hauteur = hauteur
        self.largeur = largeur
        self.echiquier = Echiquier(self.hauteur, self.largeur)

        self.fenetre = Fenetre(hauteur, largeur)

        self.joueur1 = Joueur(Couleur.BLANC)
        self.joueur2 = Joueur(Couleur.NOIR)

        self.init_position_piece (self.joueur1.tour1, Position.D4)
        self.init_position_piece (self.joueur1.cavalier1, Position.B1)
        self.init_position_piece (self.joueur1.fou1, Position.C1)
        self.init_position_piece (self.joueur1.reine, Position.D1)
        self.init_position_piece (self.joueur1.roi, Position.E1)
        self.init_position_piece (self.joueur1.fou2, Position.F1)
        self.init_position_piece (self.joueur1.cavalier2, Position.G1)
        self.init_position_piece (self.joueur1.tour2, Position.H1)
        self.init_position_piece (self.joueur1.pion1, Position.A2)
        self.init_position_piece (self.joueur1.pion2, Position.B2)
        self.init_position_piece (self.joueur1.pion3, Position.C2)
        self.init_position_piece (self.joueur1.pion4, Position.D2)
        self.init_position_piece (self.joueur1.pion5, Position.E2)
        self.init_position_piece (self.joueur1.pion6, Position.F2)
        self.init_position_piece (self.joueur1.pion7, Position.G2)
        self.init_position_piece (self.joueur1.pion8, Position.H2)

        self.init_position_piece (self.joueur2.tour1, Position.A8)
        self.init_position_piece (self.joueur2.cavalier1, Position.B8)
        self.init_position_piece (self.joueur2.fou1, Position.C8)
        self.init_position_piece (self.joueur2.reine, Position.D8)
        self.init_position_piece (self.joueur2.roi, Position.E8)
        self.init_position_piece (self.joueur2.fou2, Position.F8)
        self.init_position_piece (self.joueur2.cavalier2, Position.G8)
        self.init_position_piece (self.joueur2.tour2, Position.H8)
        self.init_position_piece (self.joueur2.pion1, Position.A7)
        self.init_position_piece (self.joueur2.pion2, Position.B7)
        self.init_position_piece (self.joueur2.pion3, Position.C7)
        self.init_position_piece (self.joueur2.pion4, Position.D7)
        self.init_position_piece (self.joueur2.pion5, Position.E7)
        self.init_position_piece (self.joueur2.pion6, Position.F7)
        self.init_position_piece (self.joueur2.pion7, Position.G7)
        self.init_position_piece (self.joueur2.pion8, Position.H7)
        
        self.liste_pieces = [self.joueur1.tour1, self.joueur1.tour2, self.joueur1.cavalier1, self.joueur1.cavalier2, self.joueur1.fou1, self.joueur1.fou2, self.joueur1.reine, self.joueur1.roi, self.joueur1.pion1, self.joueur1.pion2, self.joueur1.pion3, self.joueur1.pion4, self.joueur1.pion5, self.joueur1.pion6, self.joueur1.pion7, self.joueur1.pion8, 
            self.joueur2.tour1, self.joueur2.tour2, self.joueur2.cavalier1, self.joueur2.cavalier2, self.joueur2.fou1, self.joueur2.fou2, self.joueur2.reine, self.joueur2.roi, self.joueur2.pion1, self.joueur2.pion2, self.joueur2.pion3, self.joueur2.pion4, self.joueur2.pion5, self.joueur2.pion6, self.joueur2.pion7, self.joueur2.pion8]
    
    def changer_position_pieces (self, click_x, click_y):
        position_xy = self.afficher_position_click(click_x, click_y)
        for piece in self.liste_pieces:
            if (piece.position.position == position_xy):
                print ("changer_position_piece reussi: "+str(piece.position.position))
                break              

    def init_fond(self, dico):
        Debug.log_method("init_fond: >>>")
		#dictionnaire - ligne
        dico['A', 8] = "tour noir"
        dico['B', 8] = "cavalier noir"
        dico['C', 8] = "fou noir"
        dico['D', 8] = "reine noir"
        dico['E', 8] = "roi noir"
        dico['F', 8] = "fou noir"
        dico['G', 8] = "cavalier noir"
        dico['H', 8] = "tour noir"
        #ligne 7
        dico['A', 7] = "pion noir"
        dico['B', 7] = "pion noir"
        dico['C', 7] = "pion noir"
        dico['D', 7] = "pion noir"
        dico['E', 7] = "pion noir"
        dico['F', 7] = "pion noir"
        dico['G', 7] = "pion noir"
        dico['H', 7] = "pion noir"
        #ligne 2
        dico['A', 2] = "pion blanc"
        dico['B', 2] = "pion blanc"
        dico['C', 2] = "pion blanc"
        dico['D', 2] = "pion blanc"
        dico['E', 2] = "pion blanc"
        dico['F', 2] = "pion blanc"
        dico['G', 2] = "pion blanc"
        dico['H', 2] = "pion blanc"
        #ligne 1
        dico['A', 1] = "tour blanc"
        dico['B', 1] = "cavalier blanc"
        dico['C', 1] = "fou blanc"
        dico['D', 1] = "reine blanc"
        dico['E', 1] = "roi blanc"
        dico['F', 1] = "fou blanc"
        dico['G', 1] = "cavalier blanc"
        dico['H', 1] = "tour blanc"

    def init_position_piece (self, piece, position):
        Debug.log_method("init_position_piece: >>>")
        tuple_position = self.echiquier.get_position(position)
        contenu_position = self.echiquier.contenu[tuple_position[0]][ tuple_position[1]]
        piece.changer_position(contenu_position)	#init la position de la piece
        self.fenetre.affichePieces(piece)	#Cherche la def affichePiece dans la classe fenetre, affiche la piece et la blit, rafraichit la fenetre.

    def afficher_position_click(self, click_x, click_y):	#click_x = position souris en x et click_y = position souris en y
        Debug.log_method("afficher_position_click: >>>")
        largeur_case = self.largeur//8
        hauteur_case = self.hauteur//8
        print ("getPosition: "+str(self.echiquier.get_case_position(click_x//hauteur_case, click_y//largeur_case)))
		#print la positon de la case qu'on clique
        return self.echiquier.get_case_position(click_x//hauteur_case, click_y//largeur_case)

    def surligner_case(self, click_x, click_y):
        """ @method: surligner_case
        """
        Debug.log_method("surligner_case: >>>")
        largeur_case = self.largeur//8
        hauteur_case = self.hauteur//8
        self.fenetre.surligner_case(self.echiquier, self.echiquier.get_position(self.echiquier.get_case_position(click_x//hauteur_case, click_y//largeur_case)))
